import {ON_SORT_END, ADD_TODO, REMOVE_TASK, EDIT_TASK, SWITCH_STATUS} from '../reducers/todoReducers';

export const onSortEnd = (oldIndex, newIndex) => {
    return{
        type:ON_SORT_END,
        payload:{
            oldIndex,
            newIndex
        }
    }
}
export const addTodo = text => {
    return {
        type: ADD_TODO,
        payload: {
            id: Math.random(),
            text,
            done: false
        }
    }
}

export const removeTask = id => {
    return {
        type: REMOVE_TASK,
        payload: {
            id
        }
    }
}

export const editTask = (id, text) => {
    return {
        type: EDIT_TASK,
        payload: {
            id,
            text
        }
    }
}
export const switchStatus = (id, done) =>{
    return{
        type:SWITCH_STATUS,
        payload:{
            id,
            done
        }
    }
}

