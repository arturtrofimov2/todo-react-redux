import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import Header from "./Header";

import {addTodo} from '../../actions/todoActions';

const mapStateToProps = state => ({
    taskList: state.todo.taskList
})

const mapDispatchToProps = dispatch => ({
    addTask: bindActionCreators(addTodo, dispatch)
})

export default connect(mapStateToProps, mapDispatchToProps)(Header);