import React from 'react'
import './Header.css'

class Header extends React.Component{
    handleAddClick = () => {
        const {addTask} = this.props;
        const {value} = this.input;

        addTask(value);
        this.input.value = '';
    }
    handleEnterClick = ({key, keyCode}) => {
        if(key === 'Enter'){
            const {addTask} = this.props;
            const {value} = this.input;

            addTask(value);
            this.input.value = '';
        }
    }
    render(){;
        return(
            <header className="header">
                <h2>Todo list</h2>
                <div className="form">
                    <input type='text' placeholder="Enter you tasks" onKeyDown={this.handleEnterClick} ref={input => {this.input = input} }/>
                    <button className='btn' onClick={this.handleAddClick}>Add</button>
                </div>
            </header>
        )
    }
}
Header.defaultProps = {
    addTask:() =>{}
}
export default Header;