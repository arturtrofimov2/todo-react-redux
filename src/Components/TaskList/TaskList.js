import React from 'react'
import ListItem from './ListItem'
import './TaskList.css'
import {SortableContainer, SortableElement, arrayMove} from 'react-sortable-hoc';

const SortableItem = SortableElement(ListItem)

class TaskList extends React.Component{
    getList(){
        const{taskList, removeTask, switchStatus, editTask, onSortEnd} = this.props;
        if(taskList.length === 0) return <div className='no-tasks'>You haven't tasks!</div>
        return taskList.map((v,i) => (
            <SortableItem
                key={i}
                remove={removeTask}
                onSortEnd={onSortEnd}
                index={i}
                switchStatus={switchStatus}
                editTask = {editTask}
                text={v.text}
                number={i}
                id={v.id}
                done={v.done}
            />)
        )
    }
    render(){
        return(
            <ul className='task-list'>{this.getList()}</ul>
        )
    }
}
TaskList.defaultProps = {
    taskList: [],
    removeTask: () => {},
    editTask: () => {},
    switchStatus: () => {},
    onSortEnd: () => {}
}
export default TaskList;