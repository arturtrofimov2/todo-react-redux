import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import TaskList from './TaskList';
import {removeTask, editTask, switchStatus, onSortEnd} from '../../actions/todoActions';

const mapStateToProps = state => ({
    taskList: state.todo.taskList
})
const mapDispatchToProps = dispatch => ({
    removeTask:bindActionCreators(removeTask, dispatch),
    editTask:bindActionCreators(editTask, dispatch),
    switchStatus:bindActionCreators(switchStatus, dispatch),
   onSortEnd:bindActionCreators(onSortEnd, dispatch)
})
export default connect(mapStateToProps, mapDispatchToProps)(TaskList);
