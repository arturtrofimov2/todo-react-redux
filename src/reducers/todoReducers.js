import {arrayMove} from 'react-sortable-hoc';
export const ON_SORT_END = 'ON_SORT_END';
export const ADD_TODO = 'ADD_TODO';
export const REMOVE_TASK = 'REMOVE_TASK';
export const EDIT_TASK = 'EDIT_TASK';
export const SWITCH_STATUS = 'SWITCH_STATUS';
const initialState = {
    taskList:[
        {id:Math.random(), text:'Go to fitness club', done:false},
        {id:Math.random(), text:'Go to Supermarket', done:false},
        {id:Math.random(), text:'Go to University', done:false},
        {id:Math.random(), text:'Go to Learn React!', done:false},
        {id:Math.random(), text:'Go to Learn Redux!', done:false}
    ]
}
const actions = {
    [ON_SORT_END]: (state = initialState, action) => {
        const {payload} = action;
         return{
             ...state,
             taskList:arrayMove(state.taskList, ...payload)
         }
    },
    [ADD_TODO]: (state, action) => {
        const {payload} = action;
        return {
            ...state,
            taskList: [...state.taskList, {...payload}]
        }
    },
    [REMOVE_TASK]: (state, action) => {
        const {payload: {id}} = action;

        return {
            ...state,
            taskList: state.taskList.filter(v => v.id !== id)
        }
    },
    [EDIT_TASK]: (state, action) => {
        const {payload: {id, text}} = action;

        const elementIndex = [...state.taskList].findIndex(v => v.id === id);

        const newTaskList = [...state.taskList];
        newTaskList[elementIndex].text = text;

        return {
            ...state,
            taskList: [...newTaskList]
        }
    },
    [SWITCH_STATUS]: (state, action) => {
        const {payload:{id}} = action;

        const elementIndex = [...state.taskList].findIndex(v => v.id === id);

        const newTaskList = [...state.taskList];
        newTaskList[elementIndex].done = !newTaskList[elementIndex].done;

        return {
            ...state,
            taskList: [...newTaskList]
        }
    }
}


export default (state = initialState, action) => {
    const {type} = action;

    if(actions[type]) return actions[type](state, action);
    return state;
}