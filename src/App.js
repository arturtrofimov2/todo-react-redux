import React, { Component } from 'react';
import Header from './Components/Header';
import TaskList from './Components/TaskList';
import {arrayMove, SortableContainer} from 'react-sortable-hoc';

import './App.css';
import './styles/commons.css';
import './styles/reset.css';

const SortableList = SortableContainer(TaskList)
;
class App extends Component {
  render(){
    return(
        <div className="app-container">
          <div className="app">
            <Header />
            <SortableList
            />
          </div>
        </div>
    )
  }

}


export default App;